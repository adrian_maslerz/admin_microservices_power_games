import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/index';

import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ApiService
{
    public url: string = environment.api;
    constructor(private http: HttpClient) { }

    public post(microservice: string, url: string, body?: Object) : Observable<any>
    {
        return this.http.post(this.url.replace("MICROSERVICE", microservice) + url, body);
    }

    public put(microservice: string, url: string, body?: Object) : Observable<any>
    {
        return this.http.put(this.url.replace("MICROSERVICE", microservice) + url, body);
    }

    public get(microservice: string, url: string, params?: HttpParams, options?: any) : Observable<any>
    {
        return this.http.get(this.url.replace("MICROSERVICE", microservice) + url, { params: params, responseType: options && options["responseType"] ? options["responseType"] :  "json" });
    }

    public delete(microservice: string, url: string, body?: Object) : Observable<any>
    {
        return this.http.delete(this.url.replace("MICROSERVICE", microservice) + url, body);
    }
}
