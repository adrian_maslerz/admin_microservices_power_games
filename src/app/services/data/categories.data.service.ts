import { Injectable } from '@angular/core';
import { ApiService } from '../core/api.service';
import { Observable } from 'rxjs/index';
import { SuccessInterface } from '../../interfaces/success.interface';
import { Pagination } from '../../interfaces/pagination.interface';
import { Category } from '../../interfaces/category.interface';

@Injectable()
export class CategoriesDataService
{
    constructor(private apiService: ApiService) { }

    public getCategories(data: any) : Observable<Pagination<Category>>
    {
        return this.apiService.get("products","/categories", data);
    }

    public getCategory(id: string) : Observable<Category>
    {
        return this.apiService.get("products", "/categories/" + id);
    }

    public addCategory(data: any) : Observable<SuccessInterface>
    {
        return this.apiService.post("products", "/categories", data);
    }

    public updateCategory(id: string, data: any) : Observable<SuccessInterface>
    {
        return this.apiService.put("products", "/categories/" + id, data);
    }

    public deleteCategory(id: string) : Observable<SuccessInterface>
    {
        return this.apiService.delete("products", "/categories/" + id);
    }
}
