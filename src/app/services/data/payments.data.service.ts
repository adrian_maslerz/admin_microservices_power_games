import { Injectable } from '@angular/core';
import { ApiService } from '../core/api.service';
import { Observable } from 'rxjs/index';
import { Pagination } from '../../interfaces/pagination.interface';
import { Payment } from '../../interfaces/payment.interface';

@Injectable()
export class PaymentsDataService
{
    constructor(private apiService: ApiService) { }

    public getPayments(data: any) : Observable<Pagination<Payment>>
    {
        return this.apiService.get("payments","/payments", data);
    }

    public getPayment(id: string) : Observable<Payment>
    {
        return this.apiService.get("payments", "/payments/" + id);
    }
}
