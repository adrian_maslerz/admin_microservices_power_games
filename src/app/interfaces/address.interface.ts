export interface Address
{
    postal_code: string;
    address_line_1: string;
    address_line_2?: string;
    city: string;
    country?: string;
}
