export interface Auth
{
    token: string;
    refresh_token: string;
}
