export interface Review
{
    rate: number;
    description: string;
    approved: boolean
}
