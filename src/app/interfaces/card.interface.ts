export interface Card
{
    _id: string;
    card_id: string;
    brand: string;
    last_4: string;
    exp_month: string;
    exp_year: string;
}
