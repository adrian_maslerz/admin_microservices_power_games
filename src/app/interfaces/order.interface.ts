import { UserCopy } from './user-copy.interface';
import { OrderItem } from './order-item.interface';

export interface Order
{
    _id: string;
    items: OrderItem[];
    user: string;
    payment: string;
    user_details: UserCopy;
    total: number;
    products_count: number;
    pieces_count: number;
    created: number;
}
