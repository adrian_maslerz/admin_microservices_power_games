import { Card } from './card.interface';
import { Customer } from './customer.interface';

export interface Payment
{
    _id: string;
    created: number;
    customer: Customer;
    order: string;
    card: Card;
    currency: string;
    description: string;
    amount: string;
}
