import { Address } from './address.interface';

export interface UserCopy
{
    email: string;
    first_name: string;
    last_name: string;
    phone_number: string;
    address: Address
}
