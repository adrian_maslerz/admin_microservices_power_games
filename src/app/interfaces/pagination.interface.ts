export interface Pagination<T = any>
{
    results: T[];
    pages: number;
    total: number;
}
