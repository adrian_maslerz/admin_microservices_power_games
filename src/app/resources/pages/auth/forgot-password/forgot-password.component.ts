import { Component, OnInit } from '@angular/core';
import {
    getMessageError,
    getMessagesConfig,
    handleValidationErrorMessage,
    handleValidationStateClass
} from '../../../../utilities/form.utils';
import { HttpErrorResponse } from '@angular/common/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthDataService } from '../../../../services/data/auth.data.service';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-forgot-password',
    templateUrl: './forgot-password.component.html',
    styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit
{

    form: FormGroup;
    inProgress: boolean = false;
    error: string = '';
    formUtils = { handleValidationStateClass, handleValidationErrorMessage };
    messages = [];

    constructor(
        private router: Router,
        private authDataService: AuthDataService,
        private toastr: ToastrService
    ) {}

    ngOnInit()
    {
        //init form
        this.form = new FormGroup({
            email: new FormControl(null, [Validators.required, Validators.email]),
        });

        //messages config
        const fieldsConfig = {
            email: ['required', 'email']
        };
        this.messages = getMessagesConfig(fieldsConfig);
    }

    public onSubmit(): void
    {
        //handle invalid
        if (!this.form.valid)
            return this.form.markAllAsTouched();

        this.inProgress = true;
        this.authDataService
            .resetPassword(this.form.value)
            .subscribe(() =>
                {
                    this.inProgress = false;
                    this.toastr.success('Instructions have been sent to your email address!');
                    this.router.navigate(['/login']);
                },
                (error: HttpErrorResponse) =>
                {
                    this.error = getMessageError(error);
                    this.inProgress = false;
                });
    }
}
