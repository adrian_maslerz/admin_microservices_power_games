import { Component, OnInit } from '@angular/core';
import { AuthDataService } from '../../../services/data/auth.data.service';

@Component({
    selector: 'app-auth',
    templateUrl: './auth.component.html',
    styleUrls: ['./auth.component.scss'],
    providers: [ AuthDataService ]
})
export class AuthComponent implements OnInit
{
    constructor() { }
    ngOnInit(): void {}
}
