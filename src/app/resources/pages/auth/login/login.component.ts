import { Component, OnInit } from '@angular/core';
import {
    getMessagesConfig,
    handleValidationErrorMessage,
    handleValidationStateClass
} from '../../../../utilities/form.utils';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthDataService } from '../../../../services/data/auth.data.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit
{

    public form: FormGroup;
    public inProgress: boolean = false;
    public formUtils = { handleValidationStateClass, handleValidationErrorMessage };
    public messages = [];

    constructor(private router: Router, private authDataService: AuthDataService) {}

    ngOnInit()
    {
        //init form
        this.form = new FormGroup({
            email: new FormControl(null, [Validators.required, Validators.email]),
            password: new FormControl(null, Validators.required),
        });

        //messages config
        const fieldsConfig = {
            email: ['required', 'email'],
            password: ['required']
        };
        this.messages = getMessagesConfig(fieldsConfig);
    }

    public onSubmit(): void
    {
        //handle invalid
        if (!this.form.valid)
        {
            this.form.markAllAsTouched();
            return;
        }

        this.inProgress = true;
        this.authDataService
            .login(this.form.value)
            .subscribe(() =>
                {
                    this.inProgress = false;
                    this.router.navigate(['/']);
                },
                (error: HttpErrorResponse) => {
                    this.inProgress = false;
                });
    }
}
