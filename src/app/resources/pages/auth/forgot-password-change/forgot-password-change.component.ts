import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AuthDataService } from '../../../../services/data/auth.data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {
    getMessageError,
    getMessagesConfig,
    handleValidationErrorMessage,
    handleValidationStateClass
} from '../../../../utilities/form.utils';
import { equalToFieldValue, password } from '../../../../utilities/validators';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-forgot-password-change',
  templateUrl: './forgot-password-change.component.html',
  styleUrls: ['./forgot-password-change.component.scss']
})
export class ForgotPasswordChangeComponent implements OnInit {

    private token: string = "";
    public form: FormGroup;
    public inProgress: boolean = false;
    public error: string = '';
    public formUtils = { handleValidationStateClass, handleValidationErrorMessage };
    public messages = [];

    constructor(
        private router: Router,
        private authDataService: AuthDataService,
        private toastr: ToastrService,
        private route: ActivatedRoute
    ) {}

    ngOnInit()
    {
        //getting token
        this.token = this.route.snapshot.queryParams["token"];

        //init form
        this.form = new FormGroup({
            password: new FormControl(null, [Validators.required, Validators.minLength(8), password]),
            repeated_password: new FormControl(null, [Validators.required]),
        });

        //messages config
        const fieldsConfig = {
            password: ['required', { key: 'minlength', value: 8 }, 'password', { key: 'equalToFieldValue', message: 'Passwords don\'t match.'}],
            repeated_password: ['required', { key: 'equalToFieldValue', message: 'Passwords don\'t match.' }]
        };
        this.messages = getMessagesConfig(fieldsConfig);

        // password
        this.form.get('password')
            .valueChanges
            .subscribe(
                () =>
                {
                    const control = this.form.get('repeated_password');
                    control.setValidators([Validators.required, equalToFieldValue(this.form.get('password').value)]);
                    control.updateValueAndValidity();
                });

        this.form.get('repeated_password')
            .valueChanges
            .subscribe(() => this.form.get('repeated_password').setValidators([Validators.required, equalToFieldValue(this.form.get('password').value)]));
    }

    public onSubmit(): void
    {
        //handle invalid
        if (!this.form.valid)
            return this.form.markAllAsTouched();

        this.inProgress = true;
        this.authDataService
            .resetPasswordChange(this.form.value, this.token)
            .subscribe(() =>
                {
                    this.inProgress = false;
                    this.toastr.success('Your password has been changed!');
                    this.router.navigate(['/login']);
                },
                (error: HttpErrorResponse) =>
                {
                    this.error = getMessageError(error);
                    this.inProgress = false;
                });
    }
}
