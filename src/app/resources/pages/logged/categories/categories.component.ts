import { Component, OnInit } from '@angular/core';
import { CategoriesDataService } from '../../../../services/data/categories.data.service';

@Component({
    selector: 'app-categories',
    templateUrl: './categories.component.html',
    styleUrls: ['./categories.component.scss'],
    providers: [ CategoriesDataService ]
})
export class CategoriesComponent
{
    constructor() { }
}
