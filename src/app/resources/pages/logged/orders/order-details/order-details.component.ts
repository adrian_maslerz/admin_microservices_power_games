import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OrdersDataService } from '../../../../../services/data/orders.data.service';
import { Order } from '../../../../../interfaces/order.interface';
import { flatMap } from 'rxjs/operators';
import { PaymentsDataService } from '../../../../../services/data/payments.data.service';
import { Payment } from '../../../../../interfaces/payment.interface';
import { OrderItem } from '../../../../../interfaces/order-item.interface';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-order-details',
    templateUrl: './order-details.component.html',
    styleUrls: ['./order-details.component.scss'],
    providers: [ PaymentsDataService ]
})
export class OrderDetailsComponent implements OnInit
{
    public id: string = '';
    public order: Order;
    public payment: Payment;
    public inProgress: boolean = false;

    constructor(
        private route: ActivatedRoute,
        private ordersDataService: OrdersDataService,
        private paymentsDataService: PaymentsDataService,
        private toastr: ToastrService
    ) { }

    ngOnInit(): void
    {
        this.id = this.route.snapshot.params[ 'id' ];
        this.getData();
    }

    private getData(): void
    {
        this.ordersDataService
            .getOrder(this.id)
            .pipe(flatMap((order: Order) => {
                this.order = order;
                return this.paymentsDataService.getPayment(this.order.payment);
            }))
            .subscribe((payment: Payment) => this.payment = payment);
    }

    public onReviewApprove(item: OrderItem): void
    {
        this.inProgress = true;
        this.ordersDataService
            .updateReviewState(this.order._id, item._id, { approved: item.review.approved ? 0 : 1 })
            .subscribe(() => {
                this.inProgress = false;
                this.toastr.success('Review state updated successfully!');
                this.getData();
            })
    }
}
