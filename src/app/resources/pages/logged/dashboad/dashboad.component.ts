import { Component, OnInit, ViewChild } from '@angular/core';
import { PaymentsDataService } from '../../../../services/data/payments.data.service';
import { OrdersDataService } from '../../../../services/data/orders.data.service';
import { Payment } from '../../../../interfaces/payment.interface';

@Component({
    selector: 'app-dashboad',
    templateUrl: './dashboad.component.html',
    styleUrls: ['./dashboad.component.scss'],
    providers: [ PaymentsDataService ]
})
export class DashboadComponent implements OnInit
{

    @ViewChild(DashboadComponent, { static: true })

    public rows: Payment[] = [];
    public total: number = 0;
    public page: number = 1;
    public readonly results: number = 10;

    public table: DashboadComponent;

    constructor(
        private paymentsDataService: PaymentsDataService
    ) { }

    ngOnInit(): void
    {
        this.page = 1;
        this.getData();
    }

    public onPageChange(data): void
    {
        this.page = (data.offset + 1)
        this.getData();
    }

    private getData(): void
    {
        this.paymentsDataService
            .getPayments({
                page: this.page,
                results: this.results
            })
            .subscribe(results => {
                this.total = results.total;
                this.rows = results.results;
            })
    }
}
