import { Component, OnInit } from '@angular/core';
import { User } from '../../../../../interfaces/user.interface';
import { UserDataService } from '../../../../../services/data/user.data.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

    public user: User;
    constructor(private userDataService: UserDataService) {}

    ngOnInit()
    {
        //populating data
        this.userDataService.getUser().subscribe((user: User) => this.user = user);
    }
}
