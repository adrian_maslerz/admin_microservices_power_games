import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserDataService } from '../../../../../services/data/user.data.service';
import { ToastrService } from 'ngx-toastr';
import { getMessagesConfig } from '../../../../../utilities/form.utils';
import { HttpErrorResponse } from '@angular/common/http';
import { User } from '../../../../../interfaces/user.interface';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {

    public form: FormGroup;
    public inProgress: boolean = false;
    public messages: any[] = [];

    constructor(
        private router: Router,
        private userDataService: UserDataService,
        private toastr: ToastrService
    ) {}

    ngOnInit(): void
    {
        //init form
        this.form = new FormGroup({
            first_name: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
            last_name: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
        });

        //messages config
        const fieldsConfig = {
            first_name: ['required', { key: "maxlength", value: 50}],
            last_name: ['required', { key: "maxlength", value: 50}],
        };
        this.messages = getMessagesConfig(fieldsConfig);

        //populating data
        this.getData();
    }

    public onSubmit(): void
    {
        //handle invalid
        if (!this.form.valid)
            return this.form.markAllAsTouched();

        this.inProgress = true;
        this.userDataService
            .updateUser(this.form.value)
            .subscribe(() =>
                {
                    this.inProgress = false;
                    this.router.navigate(["/admin"]);
                    this.toastr.success("Profile updated successfully!");
                },
                (error: HttpErrorResponse) => {
                    this.inProgress = false;
                });
    }

    public getData(): void
    {
        this.userDataService
            .getUser()
            .subscribe((user: User) => {
                this.form.patchValue({
                    first_name: user.first_name,
                    last_name: user.last_name
                })
            });
    }
}
