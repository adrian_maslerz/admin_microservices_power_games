import { Component } from '@angular/core';
import { UserDataService } from '../../../../services/data/user.data.service';

@Component({
    selector: 'app-admin',
    templateUrl: './admin.component.html',
    styleUrls: ['./admin.component.scss'],
    providers: [ UserDataService ]
})
export class AdminComponent
{
    constructor() { }
}
