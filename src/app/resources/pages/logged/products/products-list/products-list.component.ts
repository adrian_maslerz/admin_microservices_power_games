import { Component, OnInit, ViewChild } from '@angular/core';
import { Category } from '../../../../../interfaces/category.interface';
import { FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { debounceTime } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { Product } from '../../../../../interfaces/product.interface';
import { ProductsDataService } from '../../../../../services/data/products.data.service';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss']
})
export class ProductsListComponent implements OnInit {

    @ViewChild(ProductsListComponent, { static: true })

    public rows: Product[] = [];
    public total: number = 0;
    public page: number = 1;
    private sort: string = "createdDESC";
    public readonly results: number = 10;

    private toDelete: string = null;
    public inProgress: boolean = false;
    public form: FormGroup
    public category: Category;

    public table: ProductsListComponent;
    private subscription: Subscription;

    constructor(
        private productsDataService: ProductsDataService,
        private toastr: ToastrService,
        private modalService: NgbModal
    ) { }

    ngOnInit(): void
    {
        this.form = new FormGroup({
            search: new FormControl("")
        });

        //on search
        this.subscription = this.form.get("search")
            .valueChanges
            .pipe(debounceTime(400))
            .subscribe(() => {
                this.page = 1;
                this.getData();
            });

        this.page = 1;
        this.getData();
    }

    ngOnDestroy(): void
    {
        if(this.subscription)
            this.subscription.unsubscribe();
    }

    public onPageChange(data): void
    {
        this.page = (data.offset + 1)
        this.getData();
    }

    public onSortChange(data): void
    {
        this.sort = data.column.prop.split(".")[0] + data.newValue.toUpperCase();
        this.page = 1;
        this.getData();
    }

    private getData(): void
    {
        this.productsDataService
            .getProducts({
                page: this.page,
                search: this.form.get("search").value,
                category: this.category ? this.category._id : null,
                sort: this.sort,
                results: this.results
            })
            .subscribe(results => {
                this.total = results.total;
                this.rows = results.results;
            })
    }

    public onDeleteModalOpen(modal, id: string): void
    {
        this.toDelete = id;
        this.modalService
            .open(modal)
            .result
            .then(
                result => {
                    this.toDelete = null;
                },
                reason => {
                    this.toDelete = null;
                }
            );
    }

    public onDelete(modal): void
    {
        if(this.toDelete)
        {
            this.inProgress = true;
            this.productsDataService
                .deleteProduct(this.toDelete)
                .subscribe(() =>
                    {
                        this.inProgress = false;
                        this.toastr.success("Product deleted!");
                        modal.dismiss();

                        this.page = 1;
                        this.onPageChange({ offset: this.page - 1 });
                    },
                    (error: HttpErrorResponse) =>
                    {
                        this.inProgress = false;
                        modal.dismiss();
                    });
        }
        else
            modal.dismiss();
    }

    public onSelectCategoryModalOpen(modal): void
    {
        this.modalService
            .open(modal,{ size: 'lg' })
            .result
            .then(result => {}, reason => {});
    }

    public onSelectedCategory(category: Category | null): void
    {
        this.category = category;
        this.getData();
    }
}
