import { Component, OnInit } from '@angular/core';
import { ProductsDataService } from '../../../../services/data/products.data.service';

@Component({
    selector: 'app-products',
    templateUrl: './products.component.html',
    styleUrls: ['./products.component.scss'],
    providers: [ ProductsDataService ]
})
export class ProductsComponent implements OnInit
{
    constructor() { }

    ngOnInit(): void {}
}
