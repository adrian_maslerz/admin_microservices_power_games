import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Category } from '../../../../../interfaces/category.interface';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { getMessagesConfig, handleValidationErrorMessage, timestampToDatepickerObject } from '../../../../../utilities/form.utils';
import { HttpErrorResponse } from '@angular/common/http';
import { ProductsDataService } from '../../../../../services/data/products.data.service';
import { fileSize, fileType, invalidDate, numeric } from '../../../../../utilities/validators';
import { Product } from '../../../../../interfaces/product.interface';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-edit-product',
    templateUrl: './edit-product.component.html',
    styleUrls: ['./edit-product.component.scss']
})
export class EditProductComponent implements OnInit
{

    public form: FormGroup;
    public inProgress: boolean = false;
    public messages: any[] = [];
    public id: string = '';
    private file: File;
    public product: Product;
    public category: Category;
    public formUtils = { handleValidationErrorMessage };

    public ageCategories: string[] = [ "7", "12", "16", "18", "No information" ];
    public languages: string[] = [ "Polish", "English", "German", "French", "Spanish", "Italian", "Japanese" ];
    public gameModes: string[] = [ "Single player", "Multiplayer", "Massive online multiplayer" ];
    public releaseTypes: string[] = [ "Basic", "DLC addition", "GOTY edition", "VR" ];
    public validMimeTypes = [
        'image/png',
        'image/jpg',
        'image/jpeg'
    ];
    public maxFileSizeMB = 5;

    constructor(
        private router: Router,
        private productsDataService: ProductsDataService,
        private toastr: ToastrService,
        private route: ActivatedRoute,
        private modalService: NgbModal
    )
    {}

    ngOnInit(): void
    {
        this.id = this.route.snapshot.params[ 'id' ];

        //init form
        this.form = new FormGroup({
            title: new FormControl(null, [Validators.required, Validators.maxLength(100)]),
            category: new FormControl(null, [Validators.required]),
            image: new FormControl(null, [Validators.required ]),
            manufacturer: new FormControl(null, [Validators.required, Validators.maxLength(100)]),
            availability: new FormControl(null, [Validators.required, Validators.min(0), numeric ]),
            intro: new FormControl(null, [Validators.required ]),
            description: new FormControl(null, [Validators.required ]),
            price: new FormControl(null, [Validators.required, Validators.min(0), numeric ]),
            age_category: new FormControl(null, [Validators.required ]),
            languages: new FormControl(null, [Validators.required ]),
            game_modes: new FormControl(null, [Validators.required ]),
            release_type: new FormControl(null, [Validators.required ]),
            premiere: new FormControl(null, [Validators.required, invalidDate ])
        });

        //messages config
        const fieldsConfig = {
            title: ['required', { key: 'maxlength', value: 100 }],
            category: ['required'],
            image: ['required', { key: 'fileSize', value: this.maxFileSizeMB + "MB"}, { key: 'fileType', value: this.validMimeTypes.join(", ")}],
            manufacturer: ['required', { key: 'maxlength', value: 100 }],
            availability: ['required', 'numeric', { key: "min", value: 0}],
            intro: ['required'],
            description: ['required'],
            price: ['required', 'numeric', { key: "min", value: 0}],
            age_category: ['required'],
            languages: ['required'],
            game_modes: ['required'],
            release_type: ['required'],
            premiere: ['required', 'invalidDate', ],
        };

        this.messages = getMessagesConfig(fieldsConfig);

        //populating data
        if (this.id)
            this.getData();
    }

    public onSubmit(): void
    {
        //handle invalid
        if (!this.form.valid)
            return this.form.markAllAsTouched();

        this.inProgress = true;
        const method = this.id ? this.productsDataService.updateProduct(this.id, this.form.value, this.file): this.productsDataService.addProduct(this.form.value, this.file);

        method
            .subscribe(() =>
                {
                    this.inProgress = false;
                    this.toastr.success('Product saved successfully!');
                    this.router.navigate(['/products']);
                },
                (error: HttpErrorResponse) => {
                    this.inProgress = false;
                });
    }

    private getData(): void
    {
        this.productsDataService
            .getProduct(this.id)
            .subscribe((product: Product) => {
                this.product = product;
                this.form.patchValue({
                    title: product.title,
                    category: product.category._id,
                    image: product.image,
                    manufacturer: product.manufacturer,
                    availability: product.availability,
                    intro: product.intro,
                    description: product.description,
                    price: product.price,
                    age_category: product.age_category,
                    languages: product.languages,
                    game_modes: product.game_modes,
                    release_type: product.release_type,
                    premiere: timestampToDatepickerObject(product.premiere)
                });
            });
    }

    public onFileChosen(file: File): void
    {
        this.file = file;
    }

    public onUpload(blobInfo, success): void
    {
        this.productsDataService
            .uploadProductPhoto(blobInfo)
            .subscribe(response => success(response.imageUrl))
    }

    public onSelectCategoryModalOpen(modal): void
    {
        this.modalService
            .open(modal,{ size: 'lg' })
            .result
            .then(result => {}, reason => {});
    }

    public onSelectedCategory(category: Category | null): void
    {
        const control = this.form.get("category");
        control.patchValue(category ? category._id : null);
        control.markAsTouched();
        this.category = category;
    }
}
