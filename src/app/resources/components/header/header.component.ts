import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from '../../../services/core/auth.service';
import { User } from '../../../interfaces/user.interface';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy
{
    public user: User;
    private subscription: Subscription;

    constructor(
        private authService: AuthService,
        private router: Router
    ) { }

    ngOnInit(): void
    {
        this.subscription = this.authService.getUserSubscription()
            .subscribe((user: User) => this.user = user);
    }

    ngOnDestroy(): void
    {
        if(this.subscription)
            this.subscription.unsubscribe();
    }

    public onLogout(): void
    {
        this.authService.logout();
        this.router.navigate(["/login"])
    }
}
