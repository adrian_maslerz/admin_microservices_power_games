import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { handleValidationErrorMessage, handleValidationStateClass } from '../../../utilities/form.utils';
import { fileSize, fileType } from '../../../utilities/validators';

@Component({
  selector: 'app-file-input',
  templateUrl: './file-input.component.html',
  styleUrls: ['./file-input.component.scss']
})
export class FileInputComponent
{
    @Input() form: FormGroup;
    @Input() name: string;
    @Input() displayName: string = this.name;
    @Input() messages = [];
    @Input() validMimeTypes: string[] = [];
    @Input() maxFileSizeMB: number = 5;
    @Input() link: string;
    @Input() image: boolean = false;
    @Output() fileChosen: EventEmitter<File> = new EventEmitter();

    public file: File;
    public formUtils = { handleValidationErrorMessage };
    private reader: FileReader = new FileReader();

    constructor() { }

    ngOnInit(): void {
        this.reader.onload = (event) => {
            this.link = <string>event.target['result'];
        };
    }

    public onFileChange(event): void
    {
        if (!event.target.files.length)
            return;

        //validating image
        this.file = null;
        const fileResource = event.target.files[0];
        const control = this.form.get(this.name);

        control.setValue(fileResource.name);
        control.setValidators([fileType(fileResource, this.validMimeTypes), fileSize(fileResource, this.maxFileSizeMB)]);
        control.markAsTouched();
        control.updateValueAndValidity();

        if(control.valid)
        {
            this.file = fileResource;
            if(this.image)
                this.reader.readAsDataURL(fileResource);
        }

        this.fileChosen.emit(this.file);
    }
}
