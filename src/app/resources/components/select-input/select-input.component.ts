import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { handleValidationErrorMessage, handleValidationStateClass } from '../../../utilities/form.utils';

@Component({
    selector: 'app-select-input',
    templateUrl: './select-input.component.html',
    styleUrls: ['./select-input.component.css']
})
export class SelectInputComponent
{
    @Input() form: FormGroup;
    @Input() name: string;
    @Input() displayName: string = this.name;
    @Input() data: Array<any> = [];
    @Input() valueKey: string = null;
    @Input() nameKey: string = null;
    @Input() messages: any[] = [];
    @Input() multiple: boolean = false;
    @Output() changed: EventEmitter<string> = new EventEmitter();

    public formUtils = { handleValidationStateClass, handleValidationErrorMessage };

    constructor() { }

    public getValue(item: Object | string) : string
    {
        return this.valueKey ? item[this.valueKey] : item;
    }

    public getName(item: Object | string) : string
    {
        return this.nameKey ? (typeof item[this.nameKey]  == "function" ? item[this.nameKey]() : item[this.nameKey]) : item;
    }

    public onChange(event): void
    {
        this.changed.emit(event.target.value);
    }
}
