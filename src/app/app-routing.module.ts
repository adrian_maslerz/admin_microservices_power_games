import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoggedComponent } from './resources/pages/logged/logged.component';
import { DashboadComponent } from './resources/pages/logged/dashboad/dashboad.component';
import { AuthComponent } from './resources/pages/auth/auth.component';
import { LoginComponent } from './resources/pages/auth/login/login.component';
import { ForgotPasswordComponent } from './resources/pages/auth/forgot-password/forgot-password.component';
import { ForgotPasswordChangeComponent } from './resources/pages/auth/forgot-password-change/forgot-password-change.component';
import { NoAuthGuard } from './guards/no-auth.guard';
import { AuthGuard } from './guards/auth.guard';
import { ProfileComponent } from './resources/pages/logged/admin/profile/profile.component';
import { EditProfileComponent } from './resources/pages/logged/admin/edit-profile/edit-profile.component';
import { ChangeEmailComponent } from './resources/pages/logged/admin/change-email/change-email.component';
import { ChangePasswordComponent } from './resources/pages/logged/admin/change-password/change-password.component';
import { AdminComponent } from './resources/pages/logged/admin/admin.component';
import { CategoriesComponent } from './resources/pages/logged/categories/categories.component';
import { CategoriesListComponent } from './resources/pages/logged/categories/categories-list/categories-list.component';
import { EditCategoryComponent } from './resources/pages/logged/categories/edit-category/edit-category.component';
import { ProductsComponent } from './resources/pages/logged/products/products.component';
import { ProductsListComponent } from './resources/pages/logged/products/products-list/products-list.component';
import { EditProductComponent } from './resources/pages/logged/products/edit-product/edit-product.component';
import { OrdersComponent } from './resources/pages/logged/orders/orders.component';
import { OrdersListComponent } from './resources/pages/logged/orders/orders-list/orders-list.component';
import { OrderDetailsComponent } from './resources/pages/logged/orders/order-details/order-details.component';

const routes: Routes = [
    //logged
    {
        path: '', component: LoggedComponent, canActivate: [AuthGuard], children: [

            //dashboard
            { path: '', component: DashboadComponent },

            //admin
            {
                path: "admin", component: AdminComponent, children: [
                    { path: '', component: ProfileComponent },
                    { path: 'edit', component: EditProfileComponent },
                    { path: 'change-email', component: ChangeEmailComponent },
                    { path: 'change-password', component: ChangePasswordComponent }
                ]
            },

            //categories
            {
                path: "categories", component: CategoriesComponent, children: [
                    { path: '', component: CategoriesListComponent },
                    { path: 'create', component: EditCategoryComponent },
                    { path: ':id', component: EditCategoryComponent }
                ]
            },

            //products
            {
                path: "products", component: ProductsComponent, children: [
                    { path: '', component: ProductsListComponent },
                    { path: 'create', component: EditProductComponent },
                    { path: ':id', component: EditProductComponent }
                ]
            },

            //orders
            {
                path: "orders", component: OrdersComponent, children: [
                    { path: '', component: OrdersListComponent },
                    { path: ':id', component: OrderDetailsComponent }
                ]
            },
        ]
    },

    //auth
    {
        path: '', component: AuthComponent, children: [
            { path: 'login', component: LoginComponent, canActivate: [NoAuthGuard] },
            { path: 'forgot-password', component: ForgotPasswordComponent, canActivate: [NoAuthGuard] },
            { path: 'password-reset', component: ForgotPasswordChangeComponent, canActivate: [NoAuthGuard] }
        ]
    },
    { path: '**', redirectTo: '/' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule
{
}
